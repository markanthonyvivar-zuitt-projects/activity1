package com.vivar;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    private static DecimalFormat df2 = new DecimalFormat("#.##");

    public static void main(String[] args) {
	// write your code here

    Scanner appScanner = new Scanner(System.in);

    String firstName;
    String lastName;
    double gradeInEnglish;
    double gradeInMathematics;
    double gradeInScience;

    System.out.println("What is your first name?");
    firstName = appScanner.nextLine().trim();

    System.out.println("What is your last name?");
    lastName = appScanner.nextLine().trim();

    System.out.println("What is your grade in English?");
    gradeInEnglish = appScanner.nextDouble();

    System.out.println("What is your grade in Mathematics?");
    gradeInMathematics = appScanner.nextDouble();

    System.out.println("What is your grade in Science?");
    gradeInScience = appScanner.nextDouble();

    System.out.println("Hello" + " " + firstName + " " + lastName + " " + "your average is:" + " " + df2.format((gradeInEnglish + gradeInScience + gradeInMathematics) / 3));






    }
}
